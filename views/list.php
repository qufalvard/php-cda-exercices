<div class="container row mx-auto">
    <?php foreach ($beanies as $beanie) { ?>
        <div class="card col m-3" style="width: 18rem;">
            <img src="<?= $beanie->getImg() ?>" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title"><?= $beanie->getName() ?></h5>
                <p class="card-text"><?= $beanie->getDescription() ?></p>
            </div>
            <p class="card-text text-center"><?= $beanie->getPrice() ?> €</p>

            <button class="btn btn-primary mb-3">
                <a class="text-white text-decoration-none" href="<?= '?page=list&addToCart=' . $beanie->getId() ?>">
                    Ajouter au panier</a>
            </button>
        </div>
    <?php } ?>
</div>
