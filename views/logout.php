<?php
session_unset();
session_destroy();
header('Location: index.php?page=login');
?>

<div class="d-flex justify-content-center container mt-3">
    <div class="alert alert-success" role="alert">
        Vous êtes bien deconnecté
    </div>
</div>
