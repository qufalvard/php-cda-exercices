<div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">nom</th>
            <th scope="col">quantité</th>
            <th scope="col">prix</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>

        <tbody>
        <h2>Vous avez <?= $compteur ?> articles dans votre panier</h2>
        <?php
        if (isset($_SESSION['cart'])) {
            $cart = $_SESSION['cart']; ?>
            <?php foreach ($cart as $item => $value) {
                foreach ($beanies as $beany) {
                    if ($beany->getId() == $item) { ?>
                        <tr>
                            <th><img style="width: 100px" src="<?= $beany->getImg() ?>"></th>
                            <td><?= $beany->getName() ?></td>
                            <td><?= $value ?></td>
                            <td><?= $value * $beany->getPrice() ?> €</td>
                            <td>
                                <a class="text-white text-decoration-none" href="<?= '?page=cart&addToCart='
                                . $bonnetsDsc[$item]['id'] ?>">
                                    ➕
                                </a>
                                <a class="text-white text-decoration-none" href="<?= '?page=cart&removeToCart='
                                . $bonnetsDsc[$item]['id'] ?>">
                                    ➖
                                </a>
                                <a class="text-white text-decoration-none" href="<?= '?page=cart&removeAll=1' ?>">
                                    🚮
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
