<?php if (!empty($_POST['password'])) {
    if ($password === $_POST['password']) {
        $_SESSION['login'] = $_POST['login'];
    } else {
        $message = "Login failed";
    }
    header('Location: index.php?page=login');
}
?>

<?php if(!empty($message)){ ?>
<div>
    <div class="d-flex justify-content-center container mt-3">
        <div class="alert alert-danger" role="alert">
            <?= $message?>
        </div>
    </div>
</div>
<?php }?>

<form class="my-4 container" method="POST">
    <h1 class="text-center">LOGIN</h1>
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>
    <button type="submit" class="btn btn-success mt-3">Submit</button>
</form>
