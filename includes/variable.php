<?php
spl_autoload_register(function ($class) {
    require_once "classes/$class.php";
});
$password = 'toto';

$beanies = [
    $beanie1 = new Beanie("Bonnet en laine", "https://www.lebonnetfrancais.fr/3925-large_default/le-bonnet-recycle.jpg",
        10, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.", 39, "coton"),
    $beanie2 = new Beanie("Bonnet en laine bio", "https://www.persomode.com/wp-content/uploads/2019/10/PS_B412_BLACK.jpg", 14, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.", 40, "lin"
    ),
    $beanie3 = new Beanie("Bonnet en laine et cachemire", "https://www.comptoir-irlandais.com/14265/bonnet-pompon-bleu-vert-inis-crafts.jpg", 20, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.", 42, "jean"
    ),
    $beanie4 = new Beanie("Bonnet arc-en-ciel", "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.wallhere.com%2Fphotos%2F51%2Fb2%2Fautumn_summer_portrait_sexy_colors_girl_beautiful_robin-506947.jpg!d&f=1&nofb=1", 12, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.", 36, "laine")
];
//var_dump($beanies);

$bonnetsDsc = [
    [
        "id" => 0,
        "name" => "Bonnet en laine",
        "price" => 10,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "img" => "https://www.lebonnetfrancais.fr/3925-large_default/le-bonnet-recycle.jpg"
    ],
    [
        "id" => "1",
        "name" => "Bonnet en laine bio",
        "price" => 14,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "img" => "https://www.persomode.com/wp-content/uploads/2019/10/PS_B412_BLACK.jpg"
    ],
    [
        "id" => "2",
        "name" => "Bonnet en laine et cachemire",
        "price" => 20,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "img" => "https://www.comptoir-irlandais.com/14265/bonnet-pompon-bleu-vert-inis-crafts.jpg"
    ],
    [
        "id" => "3",
        "name" => "Bonnet arc-en-ciel",
        "price" => 12,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "img" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.wallhere.com%2Fphotos%2F51%2Fb2%2Fautumn_summer_portrait_sexy_colors_girl_beautiful_robin-506947.jpg!d&f=1&nofb=1"
    ],
];

$bonnetsAsso = [
    "Bonnet en laine" => 10,
    "Bonnet en laine bio" => 14,
    "Bonnet en laine et cachemire" => 20,
    "Bonnet arc-en-ciel" => 12,
];
