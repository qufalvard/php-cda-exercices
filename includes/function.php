<?php

function lineTVA($price)
{
    echo "<td>" . $price * 0.8 . "</td>";
}

function lineTTC($price)
{
    if ($price <= 12) {
        echo "<td><span class=\"text-success\">$price €</span></td>";
    } elseif ($price > 12) {
        echo "<td><span class=\"text-primary\">$price €</span></td>";
    }
}

function TVA($price)
{
     return $price * 0.8;
}
