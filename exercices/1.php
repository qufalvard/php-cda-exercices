<section>
<pre class="language-html">
    <code>
        <h1>Exercice 1</h1>
        <h3>Utilisation des tableaux</h3>
            y créer un tableau PHP contenant ces produits :

            Bonnet en laine
            Bonnet en laine bio
            Bonnet en laine et cachemire
            Bonnet arc-en-ciel

            afficher ces produits dans le html en utilisant une boucle (foreach conseillé), dans une table
    </code>
</pre>

    <?php
    $bonnets = [
        'Bonnet en laine',
        'Bonnet en laine bio',
        'Bonnet en laine et cachemire',
        'Bonnet arc-en-ciel',
    ];
    ?>

    <div class="d-flex justify-content-center">
        <table class="table" border="1">
            <tr>
                <th>Bonnet(s)</th>
            </tr>
            <?php
            foreach ($bonnets as $bonnet) {
                echo "
                        <tr>
                            <td>{$bonnet}</td>
                        </tr>
                    ";
            }
            ?>
        </table>
    </div>
</section>
