<section>
<pre>
    <code>
        <h1>Exercice 3</h1>
        <h3>Conditions</h3>

        <p>
            Si un prix est inférieur ou égal à 12€, afficher le prix en vert, sinon l'afficher en bleu.
        </p>
    </code>
</pre>

    <div class="d-flex justify-content-center">
        <table class="table" border="1">
            <tr>
                <th>Bonnet(s)</th>
                <th>Price</th>
            </tr>
            <?php
            foreach ($bonnetsAsso as $bonnet => $price) { ?>
                <tr>
                    <td>
                        <?= $bonnet ?>
                    </td>
                    <td>
                        <?php if ($price <= 12) { ?>
                            <span class="text-success"> <?= $price ?>€</span>
                        <?php } else if ($price > 12) { ?>
                            <span class="text-primary"> <?= $price ?>€</span> <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</section>
