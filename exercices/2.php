<section>
<pre>
    <code>
        <h1>Exercice 2</h1>
        <h3>Tableaux imbriqués</h3>

        <p>Complexifions les données et transformons nos données en tableaux :
            Bonnet en laine : 10€
            Bonnet en laine bio : 14€
            Bonnet en laine et cachemire : 20€
            Bonnet arc-en-ciel : 12€
        Tous les produits vont également avoir la même description : Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.
        Mettre à jour l'affichage en conséquence</p>
    </code>
</pre>
    <div class="d-flex justify-content-center">
        <table class="table" border="1">
            <tr>
                <th>Bonnet(s)</th>
                <th>Price</th>
                <th>Description</th>
            </tr>
            <?php
            foreach ($bonnetsDsc as $bonnet) { ?>
                <tr>
                    <?php foreach ($bonnet as $item) { ?>
                        <td>
                            <?= $item ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
</section>
