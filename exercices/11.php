<div class="container row mx-auto">
    <?php
    $i = 1;
    foreach ($beanies as $beanie) {
        if ($i <= 3) { ?>
            <div class="card col m-3" style="width: 18rem;">
                <img src="<?=$beanie->getImg()?>" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title"><?= $beanie->getName() ?></h5>
                    <p class="card-text"><?= $beanie->getDescription() ?></p>
                </div>
            </div>
            <?php
        }
        $i++;
    } ?>
</div>
