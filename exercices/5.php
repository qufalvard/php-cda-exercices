<section>
<pre>
    <code>
        <h1>Exercice 5</h1>
        <h3>Manipulation des chaines de caractère et des variables</h3>

        <p>
            Faire une fonction qui affiche un produit (va reprendre l'affichage d'une ligne du tableau)
            Appeler cette fonction dans la boucle
        </p>
    </code>
</pre>

    <div class="d-flex justify-content-center">
        <table class="table" border="1">
            <tr>
                <th>Bonnet(s)</th>
                <th>HT</th>
                <th>TTC</th>
            </tr>
            <?php
            foreach ($bonnetsAsso as $bonnet => $price) { ?>
                <tr>
                    <td><?= $bonnet ?></td>
                    <?php lineTVA($price) ?>
                    <?php lineTTC($price) ?>
                </tr>
            <?php } ?>
        </table>
    </div>
</section>
