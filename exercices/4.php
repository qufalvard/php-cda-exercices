<section>
<pre>
    <code>
        <h1>Exercice 4</h1>
        <h3>Fonctions</h3>

        <p>
            Nos prix étaient ici TTC (incluaient la TVA), nous allons faire une fonction calculant le montant hors taxes d'un produit.
            Afficher les prix HT dans la colonne avant celle des prix TTC
        </p>
    </code>
</pre>

    <div class="d-flex justify-content-center">
        <table class="table" border="1">
            <tr>
                <th>Bonnet(s)</th>
                <th>HT</th>
                <th>TTC</th>
            </tr>
            <?php
            foreach ($bonnetsAsso as $bonnet => $price) { ?>
                <tr>
                    <td>
                        <?= $bonnet ?>
                    </td>
                    <td>
                        <?php echo TVA($price) ?>
                    </td>
                    <td>
                        <?php if ($price <= 12) { ?> <span
                            class="text-success"> <?= $price ?>€</span> <?php } else if ($price > 12) { ?> <span
                            class="text-primary"> <?= $price ?>€</span> <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</section>
