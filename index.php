<?php
if (!empty($_GET['page'])) {
    $title = $_GET['page'];
    require_once('includes/header.php');
    require_once('./views/' . $_GET["page"] . '.php');
} else {
    $title = "home";
    require_once('includes/header.php');
    require_once('./views/home.php');
}
?>

<?php require_once('includes/footer.php'); ?>
