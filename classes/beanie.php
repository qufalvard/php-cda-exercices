<?php

class Beanie
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $img;
    /**
     * @var float
     */
    private $price;
    /**
     * @var string
     */
    private $description;
    /**
     * @var float
     */
    private $size;
    /**
     * @var string
     */
    private $material;
    /**
     * @var int
     */
    private static $counter = 0;

    /**
     * Beanie constructor.
     * @param $name
     * @param $img
     * @param $price
     * @param $description
     * @param $size
     * @param $material
     */
    public function __construct($name, $img, $price, $description, $size, $material)
    {
        $this->id = ++self::$counter;
        $this->name = $name;
        $this->img = $img;
        $this->price = $price;
        $this->description = $description;
        $this->size = $size;
        $this->material = $material;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Beanie
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Beanie
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     * @return Beanie
     */
    public function setImg($img)
    {
        $this->img = $img;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Beanie
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Beanie
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     * @return Beanie
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param mixed $material
     * @return Beanie
     */
    public function setMaterial($material)
    {
        $this->material = $material;
        return $this;
    }
}
